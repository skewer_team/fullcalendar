<?php

namespace skewer\build\Page\Calendar;

use yii\web\AssetBundle;

class Asset extends AssetBundle
{
    public $sourcePath = '@skewer/build/Page/Calendar/web/';

    public $css = [
        'css/style.compile.css',
    ];

    public $js = [
        'js/script.compile.js',
    ];
}
