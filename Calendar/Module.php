<?php

namespace skewer\build\Page\Calendar;

use skewer\base\site_module;

class Module extends site_module\page\ModulePrototype implements site_module\Ajax
{
    public function actionIndex()
    {
        $this->setParser(parserTwig);
        $this->setTemplate('calendar.twig');
    }
}
