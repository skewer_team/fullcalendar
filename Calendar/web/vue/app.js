import Calendar from './Calendar.vue';
import { createApp } from 'vue';

import '../scss/calendar.scss';

createApp(Calendar).mount('#calendar');
