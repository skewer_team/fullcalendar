<?php

use skewer\base\site\Layer;

$aConfig['name'] = 'Calendar';
$aConfig['title'] = 'Модуль календаря';
$aConfig['version'] = '1.000';
$aConfig['description'] = 'Модуль календаря';
$aConfig['revision'] = '0001';
$aConfig['layer'] = Layer::PAGE;
$aConfig['languageCategory'] = 'calendar';

return $aConfig;
